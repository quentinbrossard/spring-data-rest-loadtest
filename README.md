# Minimalistic Spring boot REST + metrics + grafana + K6 Load & stress testing example

## Reference Documentation

This project combines together
- Spring boot
- Spring data REST endpoints
- prometheus metrics
- Grafana Dashboard
- K6 Load and Stress test plans

## Goals
- validate usage of all stack components together
- get an idea for expected baseline performance of a spring boot SQL DB backed ReST API.


