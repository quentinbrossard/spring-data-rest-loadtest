import http from 'k6/http';
import { check, sleep } from 'k6';
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/2.2.0/dist/bundle.js";
export const BASE_URL = 'http://localhost:8080';
export let options = {
  stages: [
    { duration: '2m', target: 50 }, // simulate ramp-up of traffic from 1 to 50 users over 2 minutes.
    { duration: '2m', target: 50 }, // stay at 50 users for 2 minutes
    { duration: '1m', target: 0 }, // ramp-down to 0 users
  ],
  thresholds: {
      http_req_duration: ['med<500'],
    http_req_duration: ['p(95)<1000'], // 99% of requests must complete below 1.5s
  },
};
export default function () {
 let myObjects = http.get(`${BASE_URL}/portfolioreports`, {}).json();
 check(myObjects, { 'retrieved portfolioreports': (obj) => obj.length > 0 });
    sleep(0.5);
 let portfolios = http.get(`${BASE_URL}/portfoliolists/1/portfolios`, {}).json();
 check(portfolios, { 'retrieved portfolios': (obj) => obj._embedded.portfolios.length > 0 });
sleep(0.5);
}

export function handleSummary(data) {
  return {
    "../../../build/load-test-summary.html": htmlReport(data),
  };
}