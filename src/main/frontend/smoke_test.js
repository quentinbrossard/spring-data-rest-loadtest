import http from 'k6/http';
import { check, sleep } from 'k6';

export const BASE_URL = 'http://localhost:8080';
export let options = {
  vus: 1,
  duration: '10s',
  thresholds: {
    http_req_duration: ['p(99)<1500'], // 99% of requests must complete below 1.5s
  },
};
export default function () {
  let myObjects = http.get(`${BASE_URL}/portfolioreports`, {}).json();
  check(myObjects, { 'retrieved portfolioreports': (obj) => obj.length > 0 });
  sleep(0.5);
  let portfolios = http.get(`${BASE_URL}/portfoliolists/1/portfolios`, {}).json();
  check(portfolios, { 'retrieved portfolios': (obj) => obj._embedded.portfolios.length > 0 });
  sleep(0.5);
}
