import http from 'k6/http';
import { check, sleep } from 'k6';
import { Rate } from 'k6/metrics';
import { htmlReport } from "https://raw.githubusercontent.com/benc-uk/k6-reporter/2.2.0/dist/bundle.js";

export let options = {

  stages: [
    { duration: '2m', target: 100 }, // below normal load
    { duration: '1m', target: 100 },
    { duration: '2m', target: 200 }, // normal load
    { duration: '3m', target: 200 },
    { duration: '2m', target: 300 }, // around the breaking point
    { duration: '1m', target: 300 },
    { duration: '2m', target: 0 }, // scale down. Recovery stage.
  ],
};

export default function () {

  const BASE_URL = 'http://localhost:8080';

  let responses = http.batch([

    [
      'GET',
      `${BASE_URL}/portfolioreports`,
      null,
      { tags: { name: 'portfolio reports' } },
    ],
    [
      'GET',
      `${BASE_URL}/portfolios?size=1000`,
      null,
      { tags: { name: 'portfolios with pagesize 1000' } },
    ],
    [
          'GET',
          `${BASE_URL}/portfoliolists/1/portfolios`,
          null,
          { tags: { name: 'portfoliolist 1 portfolios' } },
        ],
    [
      'GET',
      `${BASE_URL}/positions?size=10000`,
      null,
      { tags: { name: 'positions with pagesize 10000' } },
    ],
  ]);
  sleep(1);

}

export function handleSummary(data) {
  return {
    "../../../build/frontend/stress-test-summary.html": htmlReport(data),
  };
}