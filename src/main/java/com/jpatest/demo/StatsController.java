package com.jpatest.demo;

import com.jpatest.demo.portfolio.PortfolioReport;
import com.jpatest.demo.portfolio.PortfolioRepository;
import com.jpatest.demo.portfolio.PortfoliolistRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class StatsController {
    private final PortfoliolistRepository portfoliolistRepository;
    private final PortfolioRepository portfolioRepository;

    public StatsController(PortfoliolistRepository portfoliolistRepository, PortfolioRepository portfolioRepository) {
        this.portfoliolistRepository = portfoliolistRepository;
        this.portfolioRepository = portfolioRepository;
    }

    @GetMapping("/stats")
    public Map<String, String> getStats() {
        Map<String, String> stats = new HashMap<>();
        stats.put("Portfoliolist count", String.valueOf(portfoliolistRepository.count()));
        stats.put("Portfolios count", String.valueOf(portfolioRepository.count()));
        stats.put("Positions count", String.valueOf(portfolioRepository.allPositions().size()));

        return stats;
    }
    @GetMapping("/portfolioreports")
    public List<PortfolioReport> getPortfolioReports() {
        return portfolioRepository.portfolioReports();
    }
}
