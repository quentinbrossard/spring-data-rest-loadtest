package com.jpatest.demo.portfolio;

public enum Currency {
    CHF,
    EUR,
    USD,
    GBP
}
