package com.jpatest.demo.portfolio;

import com.github.javafaker.Faker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
@ConfigurationProperties(prefix = "dataloader")
public class DataLoader implements CommandLineRunner {

    public static final List<Integer> POSITIONS_COUNT = List.of(3, 10, 20, 25, 30, 40, 50, 100, 200);
    private int portfolioCount = 1000;
    private final PortfoliolistRepository portfoliolistRepository;
    private final PositionRepository positionRepository;

    public DataLoader(PortfoliolistRepository portfoliolistRepository, PositionRepository positionRepository) {
        this.portfoliolistRepository = portfoliolistRepository;
        this.positionRepository = positionRepository;
    }

    @Override
    public void run(String... args) {
        log.info("Loading testdata for {} portfolios", getPortfolioCount());
        Portfoliolist list1 = new Portfoliolist();
        list1.setName("Test portfoliolist 1");
        Portfoliolist list2 = new Portfoliolist();
        list2.setName("Test portfoliolist 2");
        List<Portfoliolist> lists = List.of(list1, list2);
        List<Position> positions = createPortfolios(lists);
        portfoliolistRepository.saveAll(lists);
        positionRepository.saveAll(positions);
        log.info("Done loading testdata");

    }

    private List<Position> createPortfolios(List<Portfoliolist> lists) {
        List<Position> positions = new ArrayList<>();
        for (int i = 1; i <= getPortfolioCount(); i++) {
            Portfolio p = new Portfolio();
            p.setName("Portfolio " + i);
            lists.get(i % lists.size()).addPortfolio(p);
            positions.addAll(createPositions(p, i));
        }
        return positions;
    }

    private List<Position> createPositions(Portfolio p, int i) {
        Faker faker = new Faker();
        Integer positionsCount = POSITIONS_COUNT.get((i % POSITIONS_COUNT.size()));
        List<Position> positions = new ArrayList<>(positionsCount);
        for (int j = 0; j < positionsCount; j++) {
            Position pos = new Position();
            pos.setType(PositionType.SECURITY);
            pos.setAmount(BigDecimal.valueOf(faker.number().randomNumber() / 100.0));
            pos.setCurrency(faker.options().option(Currency.class));
            pos.setSecurityId(j + 10_000_000);
            pos.setPortfolio(p);
            positions.add(pos);
        }
        return positions;
    }

    public int getPortfolioCount() {
        return portfolioCount;
    }

    public void setPortfolioCount(int portfolioCount) {
        this.portfolioCount = portfolioCount;
    }
}
