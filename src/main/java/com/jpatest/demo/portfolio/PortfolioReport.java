package com.jpatest.demo.portfolio;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class PortfolioReport {
    private Long portfolioId;
    private Currency currency;
    private BigDecimal amount;
    private Long positionCount;

    public PortfolioReport(Long portfolioId, Currency currency, BigDecimal amount, Long positionCount) {
        this.portfolioId = portfolioId;
        this.currency = currency;
        this.amount = amount;
        this.positionCount = positionCount;
    }
}
