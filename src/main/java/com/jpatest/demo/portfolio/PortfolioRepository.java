package com.jpatest.demo.portfolio;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;


@RepositoryRestResource
public interface PortfolioRepository extends PagingAndSortingRepository<Portfolio, Long> {
    @Override
    <S extends Portfolio> Iterable<S> saveAll(Iterable<S> entities);

    @Query("SELECT new com.jpatest.demo.portfolio.PortfolioReport(p.id, pos.currency, SUM(pos.amount), COUNT(pos))"+
            " FROM Position pos JOIN Portfolio p ON p = pos.portfolio GROUP BY p.id, pos.currency")
    List<PortfolioReport> portfolioReports();

    @Query("SELECT p FROM Position p")
    List<Position> allPositions();

}
