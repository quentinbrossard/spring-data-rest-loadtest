package com.jpatest.demo.portfolio;

import com.jpatest.demo.common.BaseEntity;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
public class Portfoliolist extends BaseEntity {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    @ColumnDefault("FALSE")
    private Boolean shared;

    @ManyToMany(cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE,
    })
    private Set<Portfolio> portfolios = new HashSet<>();

    public void addPortfolio(Portfolio portfolio) {
        portfolios.add(portfolio);
       // portfolio.getPortfoliolists().add(this);
    }

    public void removePortfolio(Portfolio portfolio) {
        portfolios.remove(portfolio);
       // portfolio.getPortfoliolists().remove(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Portfoliolist)) return false;
        return id != null && id.equals(((Portfoliolist) o).getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
