package com.jpatest.demo.portfolio;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PortfoliolistController {

    private final PortfoliolistService portfoliolistService;

    public PortfoliolistController(PortfoliolistService portfoliolistService) {
        this.portfoliolistService = portfoliolistService;
    }

    @GetMapping("/allpositions")
    public List<Position> allPositions() {
        return portfoliolistService.getAllPositions();
    }
 }
