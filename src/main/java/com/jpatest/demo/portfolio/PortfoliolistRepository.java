package com.jpatest.demo.portfolio;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource
public interface PortfoliolistRepository extends PagingAndSortingRepository<Portfoliolist, Long> {

    @Override
    Page<Portfoliolist> findAll(Pageable pageable);

    @Override
    Optional<Portfoliolist> findById(Long id);
}
