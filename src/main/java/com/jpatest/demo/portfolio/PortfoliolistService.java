package com.jpatest.demo.portfolio;

import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class PortfoliolistService {
    private final PortfolioRepository portfolioRepository;

    public PortfoliolistService(PortfolioRepository portfolioRepository) {
        this.portfolioRepository = portfolioRepository;
    }


    public List<Position> getAllPositions() {
        return portfolioRepository.allPositions();
    }
}
