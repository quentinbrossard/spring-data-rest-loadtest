package com.jpatest.demo.portfolio;

import com.jpatest.demo.common.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
public class Position extends BaseEntity {

    @Id
    @GeneratedValue
    private Long id;
    private PositionType type;

    private Integer securityId;
    private BigDecimal amount;
    private Currency currency;
    @ManyToOne(fetch = FetchType.LAZY)
    private Portfolio portfolio;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Position )) return false;
        return id != null && id.equals(((Position) o).getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
