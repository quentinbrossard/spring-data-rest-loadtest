package com.jpatest.demo.portfolio;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.lang.NonNull;

public interface PositionRepository extends PagingAndSortingRepository<Position, Long> {
    @Override
    @NonNull
    <S extends Position> Iterable<S> saveAll(@NonNull Iterable<S> entities);

    @Override
    Iterable<Position> findAll();
}
